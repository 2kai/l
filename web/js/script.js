// request permission on page load
document.addEventListener('DOMContentLoaded', function () {
    if (Notification.permission !== "granted")
        Notification.requestPermission();
});

$(document).ready(function () {
    function desktopAlert(title, body) {
        if (!Notification) {
            return;
        }

        if (Notification.permission !== "granted")
            Notification.requestPermission();
        else {
            var notification = new Notification(title, {body: body});

            setTimeout(notification.close.bind(notification), 3000);
            notification.onclick = function () {
                window.focus();
            };
        }
    }

    function startPomodoroTimer(endTime, status) {
        $('#pomodoro-timer').addClass(status).countdown(endTime, function (event) {
            $(this).text(event.strftime('%M:%S'));
        }).on('finish.countdown', function () {
            $.get(pomodoroNextTimerUrl, function (response) {
                    var audio = new Audio('/sounds/alarmwatch.ogg.ogx');
                    audio.play();

                    desktopAlert('2kai', 'Your time is up');

                    $('#pomodoro-timer')
                        .removeClass()
                        .addClass(response.current_timer_status)
                        .countdown(new Date(response.current_timer))
                    ;
                }
            );
        });
    }

    if (typeof pomodoroTimer !== 'undefined') {
        startPomodoroTimer(pomodoroTimer, pomodoroTimerStatus);
    }
});