<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class PomodoroController extends Controller
{
    /**
     * @Route("/dashboard/pomodoro-next", name="pomodoro_next")
     */
    public function pomodoroNextAction(Request $request)
    {
        $this->get('pomodoro_service')->nextTimer($this->getUser());

        if ($request->isXmlHttpRequest()) {
            return new JsonResponse([
                'current_timer_status' => $this->get('pomodoro_service')->getCurrentTimerStatus($this->getUser()),
                'current_timer'        => $this->get('pomodoro_service')->getCurrentTimerEndTime($this->getUser()),
            ]);
        } else {
            return $this->redirectToRoute('dashboard');
        }
    }

    /**
     * @Route("/dashboard/pomodoro-stop", name="pomodoro_stop")
     */
    public function pomodoroStopAction()
    {
        $this->get('pomodoro_service')->stopTimer($this->getUser());

        return $this->redirectToRoute('dashboard');
    }
}