<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DashboardController extends Controller
{
    /**
     * @Route("/dashboard/", name="dashboard")
     */
    public function indexAction(Request $request)
    {
        $tasks = $this->getDoctrine()->getRepository('AppBundle:Task')->findAll();

        $isTimerStarted     = $this->get('pomodoro_service')->isTimerStarted($this->getUser());
        $currentTimer       = $this->get('pomodoro_service')->getCurrentTimerEndTime($this->getUser());
        $currentTimerStatus = $this->get('pomodoro_service')->getCurrentTimerStatus($this->getUser());

        return $this->render('dashboard/index.html.twig', [
            'tasks'                => $tasks,
            'is_timer_started'     => $isTimerStarted,
            'current_timer_status' => $currentTimerStatus,
            'current_timer'        => $currentTimer,
        ]);
    }
}