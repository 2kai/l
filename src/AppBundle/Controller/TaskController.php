<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Task;
use AppBundle\Entity\TaskLog;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TaskController extends Controller
{
    /**
     * @Route("/dashboard/task/start/{task_id}", name="task_start", requirements={"task_id": "\d+"})
     */
    public function startAction($task_id)
    {
        $task = $this->getDoctrine()->getRepository('AppBundle:Task')->find($task_id);

        $taskLog = new TaskLog();
        $taskLog->setTask($task)
            ->setStartDate(new \DateTime());

        $em = $this->getDoctrine()->getManager();

        $em->persist($taskLog);
        $em->flush();

        return $this->redirectToRoute('dashboard');

    }

    /**
     * @Route("/dashboard/task/stop/{task_id}", name="task_stop", requirements={"task_id": "\d+"})
     */
    public function stopAction($task_id)
    {
        $task = $this->getDoctrine()->getRepository('AppBundle:Task')->find($task_id);

        $taskLogs = $task->getUnfinishedLogs();

        $em = $this->getDoctrine()->getManager();

        foreach ($taskLogs as $taskLog) {
            /** @var TaskLog $taskLog */
            $taskLog->setFinishDate(new \DateTime());

            $em->persist($taskLog);
        }

        $em->flush();

        return $this->redirectToRoute('dashboard');
    }
}