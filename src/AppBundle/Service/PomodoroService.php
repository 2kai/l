<?php

namespace AppBundle\Service;

use AppBundle\Entity\Pomodoro;
use AppBundle\Entity\User;
use AppBundle\Entity\UserSetting;
use Doctrine\ORM\EntityManager;

class PomodoroService
{
    const TIMER_LENGTH      = 25;
    const BREAK_LENGTH      = 5;
    const LONG_BREAK_LENGTH = 15;

    const MAX_WORK_TIMERS_COUNT = 4;

    const STATE_IDLE       = 'idle';
    const STATE_WORK       = 'work';
    const STATE_BREAK      = 'break';
    const STATE_LONG_BREAK = 'long_break';

    private $em;

    /**
     * PomodoroService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isTimerStarted(User $user)
    {
        $currentState = $this->getCurrentStateSetting($user);

        return $currentState && $currentState != self::STATE_IDLE;
    }

    /**
     * @param User $user
     */
    public function nextTimer(User $user)
    {
        $currentStateSetting = $this->getCurrentStateSetting($user);

        if (!$currentStateSetting) {
            $currentStateSetting = new UserSetting();
            $currentStateSetting->setUser($user)
                ->setSetting('pomodoro.current_state')
                ->setValue(self::STATE_IDLE);
        }

        switch ($currentStateSetting->getValue()) {
            case self::STATE_IDLE:
            case self::STATE_BREAK:
            case self::STATE_LONG_BREAK: {
                $newState = self::STATE_WORK;
                break;
            }
            case self::STATE_WORK: {
                $finishedTimersSetting = $this->getFinishedTimersSetting($user);
                if (!$finishedTimersSetting) {
                    $finishedTimersSetting = new UserSetting();
                    $finishedTimersSetting->setUser($user)
                        ->setSetting('pomodoro.finished_timers')
                        ->setValue(0);
                }

                $finishedTimersCount = (int) $finishedTimersSetting->getValue() + 1;

                if ($finishedTimersCount >= self::MAX_WORK_TIMERS_COUNT) {
                    $newState            = self::STATE_LONG_BREAK;
                    $finishedTimersCount = 0;
                } else {
                    $newState = self::STATE_BREAK;
                }

                $finishedTimersSetting->setValue($finishedTimersCount);
                $this->em->persist($finishedTimersSetting);

                $startTimeSetting = $this->getStartTimeSetting($user);

                $startTime  = new \DateTime($startTimeSetting->getValue());
                $finishTime = (new \DateTime($startTimeSetting->getValue()))
                    ->add(new \DateInterval('PT' . round(self::TIMER_LENGTH) . 'M'));

                $pomodoro = new Pomodoro();
                $pomodoro->setStartDate($startTime)
                    ->setFinishDate($finishTime);
                $this->em->persist($pomodoro);

                break;
            }
            default: {
                $newState = self::STATE_IDLE;
            }
        }

        $currentStateSetting->setValue($newState);
        $this->em->persist($currentStateSetting);

        $startTimeSetting = $this->getStartTimeSetting($user);
        if (!$startTimeSetting) {
            $startTimeSetting = new UserSetting();
            $startTimeSetting->setUser($user)
                ->setSetting('pomodoro.start_time');
        }

        $startTimeSetting->setValue(date('c'));
        $this->em->persist($startTimeSetting);

        $this->em->flush();
    }

    /**
     * @param User $user
     */
    public function stopTimer(User $user)
    {
        $currentStateSetting = $this->getCurrentStateSetting($user);
        if ($currentStateSetting) {
            $this->em->remove($currentStateSetting);
        }

        $startTimeSetting = $this->getStartTimeSetting($user);
        if ($startTimeSetting) {
            $this->em->remove($startTimeSetting);
        }

        $finishedTimersSetting = $this->getFinishedTimersSetting($user);
        if ($finishedTimersSetting) {
            $this->em->remove($finishedTimersSetting);
        }

        $this->em->flush();
    }

    /**
     * @param User $user
     * @return int
     */
    public function getCurrentTimerEndTime(User $user)
    {
        $currentStateSetting = $this->getCurrentStateSetting($user);
        if (!$currentStateSetting) {
            return time() * 1000;
        }

        switch ($currentStateSetting->getValue()) {
            case self::STATE_WORK: {
                $timerLength = self::TIMER_LENGTH;
                break;
            }
            case self::STATE_BREAK: {
                $timerLength = self::BREAK_LENGTH;
                break;
            }
            case self::STATE_LONG_BREAK: {
                $timerLength = self::LONG_BREAK_LENGTH;
                break;
            }
            default: {
                $timerLength = 0;
            }
        }

        $startTimeSetting = $this->getStartTimeSetting($user);

        $endTime = $startTimeSetting ? strtotime($startTimeSetting->getValue()) + $timerLength * 60 : time();

        // * 1000 is need for js
        return $endTime * 1000;
    }

    /**
     * @param User $user
     * @return string
     */
    public function getCurrentTimerStatus(User $user)
    {
        $currentStateSetting = $this->getCurrentStateSetting($user);
        if (!$currentStateSetting) {
            return self::STATE_IDLE;
        } else {
            return $currentStateSetting->getValue();
        }
    }

    /**
     * @param User $user
     * @return null|UserSetting
     */
    private function getCurrentStateSetting(User $user)
    {
        $repository = $this->em->getRepository('AppBundle:UserSetting');

        return $repository->findOneBy([
            'user' => $user->getId(),
            'setting' => 'pomodoro.current_state',
        ]);
    }

    /**
     * @param User $user
     * @return null|UserSetting
     */
    private function getStartTimeSetting(User $user)
    {
        $repository = $this->em->getRepository('AppBundle:UserSetting');

        return $repository->findOneBy([
            'user'    => $user->getId(),
            'setting' => 'pomodoro.start_time',
        ]);
    }

    /**
     * @param User $user
     * @return null|UserSetting
     */
    private function getFinishedTimersSetting(User $user)
    {
        $repository = $this->em->getRepository('AppBundle:UserSetting');

        return $repository->findOneBy([
            'user' => $user->getId(),
            'setting' => 'pomodoro.finished_timers',
        ]);
    }
}